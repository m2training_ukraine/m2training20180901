<?php
/**
 * Event manager
 * Used to dispatch global events
 *
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Magento\Framework\Event;

class Manager implements ManagerInterface
{
    /**
     * Events cache
     *
     * @var array
     */
    protected $_events = [];

    /**
     * Event invoker
     *
     * @var InvokerInterface
     */
    protected $_invoker;

    /**
     * Event config
     *
     * @var ConfigInterface
     */
    protected $_eventConfig;

    /**
     * @param InvokerInterface $invoker
     * @param ConfigInterface $eventConfig
     */
    public function __construct(InvokerInterface $invoker, ConfigInterface $eventConfig)
    {
        $this->_invoker = $invoker;
        $this->_eventConfig = $eventConfig;
    }

    /**
     * Dispatch event
     *
     * Calls all observer callbacks registered for this event
     * and multiple observers matching event name pattern
     *
     * @param string $eventName
     * @param array $data
     * @return void
     */
    public function dispatch($eventName, array $data = [])
    {
        $eventName = mb_strtolower($eventName);
        foreach ($this->_eventConfig->getObservers($eventName) as $observerConfig) {
            $container_one = new \Magento\Framework\Event($data);
            $container_one->setName($eventName);

            $container_two = new Observer();
            $container_two->setData(array_merge(['event' => $container_one], $data));

            $this->_invoker->dispatch($observerConfig, $container_two);
        }
    }
}
