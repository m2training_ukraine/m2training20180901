<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\DiFactory\Model;

class TestFactory
{
    protected $nonInjectableClass;
    protected $nonInjectableClassFactory;
    protected $preferenceInterfaceFactory;

    public function __construct(
        \Training\DiFactory\Model\NonInjectableClass $nonInjectableClass,
        \Training\DiFactory\Model\NonInjectableClassFactory $nonInjectableClassFactory,
        \Training\Preference\Model\PreferenceInterfaceFactory $preferenceInterfaceFactory
    ) {
        $this->nonInjectableClass = $nonInjectableClass;
        $this->nonInjectableClassFactory = $nonInjectableClassFactory;
        $this->preferenceInterfaceFactory = $preferenceInterfaceFactory;
    }

    public function log()
    {
        echo get_class($this->nonInjectableClass)."</br>\n";
        echo get_class($this->nonInjectableClassFactory)."</br>\n";

        $createdClass = $this->nonInjectableClassFactory->create();
        echo get_class($createdClass)."</br>\n";
        echo "</br>\n";
        $createdClass->log();
        echo "</br>\n";
        $createdClassConfigured = $this->nonInjectableClassFactory->create([
            'list' => [
                'custom_item_1' => 'fdvgshjghghfd',
                'custom_item_2' => 'fdvgshjghghfd',
                'custom_item_3' => 'fdvgshjghghfd',
            ],
            'name' => 'nonInjectableClass CUSTOM',
        ]);
        $createdClassConfigured->log();


        echo "</br>\n";
        echo "Factory For Interface";
        echo "</br>\n";
        echo get_class($this->preferenceInterfaceFactory)."</br>\n";
        $preference = $this->preferenceInterfaceFactory->create();
        echo get_class($preference)."</br>\n";
    }

}
