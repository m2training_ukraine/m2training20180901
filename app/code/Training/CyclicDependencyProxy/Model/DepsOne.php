<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\CyclicDependencyProxy\Model;

class DepsOne
{
    protected $dependencyTwo;
    protected $registry;

    public function __construct(
        DepsTwo $dependencyTwo,
        \Magento\Framework\Registry $registry
    ) {
        $this->dependencyTwo = $dependencyTwo;
        $this->registry = $registry;
    }

    public function log()
    {
        echo get_class($this->dependencyTwo)."</br>\n";
        echo get_class($this->registry)."</br>\n";
    }

}
