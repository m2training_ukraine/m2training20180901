<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\CyclicDependencyProxy\Model;

class Test
{
    protected $dependencyOne;
    protected $registry;

    public function __construct(
        DepsOne $dependencyOne,
        \Magento\Framework\Registry $registry
    ) {
        $this->dependencyOne = $dependencyOne;
        $this->registry = $registry;
    }

    public function log()
    {
        echo get_class($this->dependencyOne)."</br>\n";
        echo get_class($this->registry)."</br>\n";
    }

}
