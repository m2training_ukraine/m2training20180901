<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\CyclicDependencyProxy\Model;

class DepsTwo
{
    protected $test;

    public function __construct(
        Test $test
    ) {
        $this->test = $test;
    }

    public function log()
    {
        echo get_class($this->test)."</br>\n";
    }

}
