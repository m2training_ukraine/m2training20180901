define([
    'uiComponent',
    'jquery',
    'ko',
    'mage/mage',
    'mage/decorate'
], function (Component, $, ko) {
    'use strict';

    return Component.extend({
        canShowFlag: false,
        canShowFlagObservable: ko.observable(false),
        getSecondTemplate: function () {
            return this.template2;
        },
        canShow: function () {
            return this.canShowFlag;
        },
        onClick: function () {
//            this.canShowFlag = !this.canShowFlag;
//            console.log(this.canShowFlag);
            this.canShowFlagObservable(!this.canShowFlagObservable());
            console.log(this.canShowFlagObservable());
        }
    });
});
