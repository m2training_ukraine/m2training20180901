<?php

namespace Training\Observers\Observer;

use Magento\Framework\Event\ObserverInterface;

class BeforeCmsPageSave implements ObserverInterface
{
    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $page = $observer->getEvent()->getData('data_object');
        $page->setContent($page->getContent() . '<br>CUSTOM STRING');
    }
}
