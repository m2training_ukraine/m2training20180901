<?php

namespace Training\Plugins\Plugin\Api;

class PageRepository
{
    public function beforeSave(\Magento\Cms\Api\PageRepositoryInterface $subject, \Magento\Cms\Api\Data\PageInterface $page)
    {
        $page->setContent('CUSTOM STRING <br>' . $page->getContent());
        return [$page];
    }
}
