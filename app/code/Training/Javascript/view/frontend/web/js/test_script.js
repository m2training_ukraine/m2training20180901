define([
    'jquery'
], function ($) {
    'use strict';
    
    return function (config, element) {
        console.log(config);
        console.log(element);
        $(element).click(function () {
            alert(config.alert_text);
        });
    }
});
