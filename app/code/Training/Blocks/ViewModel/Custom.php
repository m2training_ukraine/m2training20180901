<?php

namespace Training\Blocks\ViewModel;

class Custom implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    private $catalogHelper;
    private $urlBuilder;

    public function __construct(
        \Magento\Catalog\Helper\Data $catalogHelper,
        \Magento\Framework\UrlInterface $urlBuilder
    ) {
        $this->catalogHelper = $catalogHelper;
        $this->urlBuilder = $urlBuilder;
    }
    
    public function canShowLink()
    {
        return $this->catalogHelper->isPriceGlobal();
    }
    
    public function getActionUrl()
    {
        return $this->urlBuilder->getUrl('training_blocks/index/page'); // return 'http://magento.test/testblock/index/page';
    }
}
