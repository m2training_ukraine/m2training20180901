<?php

namespace Training\Blocks\Block;


class Custom extends \Magento\Framework\View\Element\Template
{
    private $catalogHelper;

    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Catalog\Helper\Data $catalogHelper,
        array $data = array()
    ) {
        $this->catalogHelper = $catalogHelper;
        parent::__construct($context, $data);
    }
    
    public function canShowLink()
    {
        return $this->catalogHelper->isPriceGlobal();
    }
    
    public function getActionUrl()
    {
        return $this->_urlBuilder->getUrl('training_blocks/index/page'); // return 'http://magento.test/testblock/index/page';
    }
}
