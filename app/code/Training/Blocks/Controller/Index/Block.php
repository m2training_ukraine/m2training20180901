<?php

namespace Training\Blocks\Controller\Index;


class Block extends \Magento\Framework\App\Action\Action
{
    private $pageResultFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
    }
    
    public function execute()
    {
        $result = $this->pageResultFactory->create(true); // true is important
        $layout = $result->getLayout();
        
        // $layout = $this->_view->getLayout(); // alternative way
        
        $block = $layout->createBlock(\Training\Blocks\Block\Custom::class);
        $block->setTemplate('Training_Blocks::custom-block.phtml');
        $html = $block->toHtml();
        
        // use Raw result object OR Json result object to return $html
        return $result;
    }
}
