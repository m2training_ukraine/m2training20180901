<?php

namespace Training\Blocks\Controller\Index;


class Layout extends \Magento\Framework\App\Action\Action
{
    private $layoutResultFactory;
    private $jsonResultFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $layoutResultFactory,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->layoutResultFactory = $layoutResultFactory;
        $this->jsonResultFactory = $jsonResultFactory;
    }
    
    public function execute()
    {
        $result = $this->layoutResultFactory->create();
        $html = $result->getLayout()->getOutput();
        
        $result = $this->jsonResultFactory->create();
        $result->setData(['html' => $html]);
        return $result;
    }
}
