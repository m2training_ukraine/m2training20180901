<?php

namespace Training\Blocks\Controller\Index;


class Page extends \Magento\Framework\App\Action\Action
{
    private $pageResultFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
    }
    
    public function execute()
    {
        $result = $this->pageResultFactory->create();
        return $result;
    }
}
