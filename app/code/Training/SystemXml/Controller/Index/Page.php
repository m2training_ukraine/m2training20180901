<?php

namespace Training\Controllers\Controller\Index;


class Page extends \Magento\Framework\App\Action\Action
{
    const XML_PATH_ENABLED = 'test_system/general/enabled';
    const XML_PATH_TEXT_VALUE = 'test_system/general/text_filed';
    
    private $pageResultFactory;
    private $scopeConfig;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
        $this->scopeConfig = $scopeConfig;
    }
    
    public function execute()
    {
        
        print_r($this->scopeConfig->getValue(self::XML_PATH_TEXT_VALUE, \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        print_r($this->scopeConfig->isSetFlag(self::XML_PATH_ENABLED, \Magento\Store\Model\ScopeInterface::SCOPE_STORE));
        
        exit();
        $result = $this->pageResultFactory->create();
        return $result;
    }
}
