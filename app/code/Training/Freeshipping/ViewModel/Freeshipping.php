<?php

namespace Training\Freeshipping\ViewModel;

class Freeshipping implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    const XML_PATH_FREESHIPPING_AMOUNT = 'carriers/freeshipping/free_shipping_subtotal';
    
    private $checkoutSession;
    private $checkoutHelper;
    private $scopeConfig;

    public function __construct(
        \Magento\Checkout\Model\Session $checkoutSession,
        \Magento\Checkout\Helper\Data $checkoutHelper,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->checkoutSession = $checkoutSession;
        $this->checkoutHelper = $checkoutHelper;
        $this->scopeConfig = $scopeConfig;
    }
    
    public function isFreeshiping()
    {
        return ($this->getCartAmount() >= $this->getFreeshippingAmount());
    }
    
    public function getAmountLeft()
    {
        return $this->checkoutHelper->formatPrice($this->getFreeshippingAmount() - $this->getCartAmount());
    }

    private function getFreeshippingAmount()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    private function getCartAmount()
    {
        return $this->checkoutSession->getQuote()->getSubtotal();
    }
}
