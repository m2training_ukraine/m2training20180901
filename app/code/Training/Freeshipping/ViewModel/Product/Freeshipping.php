<?php

namespace Training\Freeshipping\ViewModel\Product;

class Freeshipping implements \Magento\Framework\View\Element\Block\ArgumentInterface
{
    const XML_PATH_FREESHIPPING_AMOUNT = 'carriers/freeshipping/free_shipping_subtotal';
    
    private $coreRegistry;
    private $scopeConfig;

    public function __construct(
        \Magento\Framework\Registry $coreRegistry,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
    ) {
        $this->coreRegistry = $coreRegistry;
        $this->scopeConfig = $scopeConfig;
    }
    
    public function canShow()
    {
        return ($this->getCurrentProductPrice() >= $this->getFreeshippingAmount());
    }

    private function getFreeshippingAmount()
    {
        return $this->scopeConfig->getValue(self::XML_PATH_FREESHIPPING_AMOUNT, \Magento\Store\Model\ScopeInterface::SCOPE_STORE);
    }

    private function getCurrentProductPrice()
    {
        $product = $this->coreRegistry->registry('current_product');
        if (!$product) {
            return 0;
        }
        return $product->getFinalPrice();
    }
}
