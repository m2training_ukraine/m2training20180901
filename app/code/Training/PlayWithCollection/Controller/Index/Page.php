<?php

namespace Training\PlayWithCollection\Controller\Index;


class Page extends \Magento\Framework\App\Action\Action
{
    private $pageResultFactory;
    private $pageCollectionFactory;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
        $this->pageCollectionFactory = $pageCollectionFactory;
    }
    
    public function execute()
    {
        $pageCollection = $this->pageCollectionFactory->create();
        $pageCollection->addOrder('page_id');
//        $pageCollection->addFieldToFilter('page_id', 2);
//        $pageCollection->addFieldToFilter('page_id', ['eq' => 2]);
//        $pageCollection->addFieldToFilter('page_id', ['in' => [2,3]]);
        $pageCollection->addFieldToSelect(['title', 'identifier']);
        $pageCollection->setPageSize(2);
        $pageCollection->setCurPage(2);
        
        echo $pageCollection->getSelect();
        echo "<br>";
        foreach ($pageCollection as $page) {
            echo $page->getId()."<br>";
            echo $page->getTitle()."<br>";
            echo "<br>";
        }
        
        echo $pageCollection->getSelect();
        
        exit();
    }
}
