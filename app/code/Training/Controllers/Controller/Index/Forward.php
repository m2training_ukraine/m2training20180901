<?php

namespace Training\Controllers\Controller\Index;


class Forward extends \Magento\Framework\App\Action\Action
{
    private $forwardResultFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\ForwardFactory $forwardResultFactory
    ) {
        parent::__construct($context);
        $this->forwardResultFactory = $forwardResultFactory;
    }
    
    public function execute()
    {
        $result = $this->forwardResultFactory->create();
        $result->setModule('customer');
        $result->setController('account');
        $result->forward('login');
        return $result;
    }
}
