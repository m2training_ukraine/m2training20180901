<?php

namespace Training\Controllers\Controller\Index;


class Json extends \Magento\Framework\App\Action\Action
{
    private $jsonResultFactory;
    
    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory
    ) {
        parent::__construct($context);
        $this->jsonResultFactory = $jsonResultFactory;
    }
    
    public function execute()
    {
        $result = $this->jsonResultFactory->create();
        $result->setData(['a' => 1, 'b' => ['dasdas', 'dsda']]);
        return $result;
    }
}
