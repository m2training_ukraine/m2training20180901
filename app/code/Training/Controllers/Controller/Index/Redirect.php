<?php

namespace Training\Controllers\Controller\Index;


class Redirect extends \Magento\Framework\App\Action\Action
{
    public function execute()
    {
//        echo $this->getRequest()->getFullActionName();
//        exit();
        $result = $this->resultRedirectFactory->create();
        $result->setPath('customer/account/login');
        return $result;
    }
}
