<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\DiType\Model;

class TestType
{
    protected $design;
    protected $preference;
    protected $registry;

    public function __construct(
        \Magento\Framework\View\DesignInterface $design,
        \Training\Preference\Model\PreferenceInterface $preference,
        \Magento\Framework\Registry $registry
    ) {
        $this->design = $design;
        $this->preference = $preference;
        $this->registry = $registry;
    }

    public function log()
    {
        echo get_class($this->design)."</br>\n";
        echo get_class($this->preference)."</br>\n";
        echo get_class($this->registry)."</br>\n";
    }

}
