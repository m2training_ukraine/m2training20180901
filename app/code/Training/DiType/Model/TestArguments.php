<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\DiType\Model;

class TestArguments
{
    protected $preference;
    protected $preferenceCustom;
    protected $name;
    protected $number;
    protected $list;

    public function __construct(
        \Training\Preference\Model\PreferenceInterface $preference,
        \Training\Preference\Model\PreferenceInterface $preferenceCustom,
        $name,
        int $number,
        array $list
    ) {
        $this->preference = $preference;
        $this->preferenceCustom = $preferenceCustom;
        $this->name = $name;
        $this->number = $number;
        $this->list = $list;
    }

    public function log()
    {
        echo get_class($this->preference)."</br>\n";
        echo get_class($this->preferenceCustom)."</br>\n";
        var_dump($this->name);
        echo "</br>\n";

        var_dump($this->number);
        echo "</br>\n";

        var_dump($this->list);
        echo "</br>\n";
    }

}
