<?php

namespace Training\AddToCartRestriction\Plugin\Block;

use Magento\Customer\Model\Context;

class HideAddToCart
{
    /**
     * @var \Magento\Framework\App\Http\Context
     */
    protected $httpContext;

    /**
     * @param \Magento\Framework\App\Http\Context $httpContext
     */
    public function __construct(
        \Magento\Framework\App\Http\Context $httpContext
    ) {
        $this->httpContext = $httpContext;
    }

    public function beforeToHtml(\Magento\Catalog\Block\Product\View $subject)
    {
        if (!$this->httpContext->getValue(Context::CONTEXT_AUTH)) {
            if ($subject->getNameInLayout() == 'product.info.addtocart.additional'
                || $subject->getNameInLayout() == 'product.info.addtocart'
            ) {
                $subject->setTemplate('');
            }
        }
    }
}
