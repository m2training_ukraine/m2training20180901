<?php
/**
 * Copyright © Magento, Inc. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Training\Preference\Model;

interface PreferenceInterface
{
    public function create();

    public function get();

}
