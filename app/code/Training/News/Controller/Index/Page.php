<?php

namespace Training\News\Controller\Index;


class Page extends \Magento\Framework\App\Action\Action
{
    private $pageResultFactory;
    private $postFactory;
    private $postRepository;
    private $searchCriteriaBuilder;
    private $filterBuilder;
    private $sortOrderBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory,
        \Training\News\Api\Data\PostInterfaceFactory $postFactory,
        \Training\News\Api\PostRepositoryInterface $postRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder
        
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
        $this->postFactory = $postFactory;
        $this->postRepository = $postRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }
    
    public function execute()
    {
        
        // CREATE VIA REPOSITORY
        $newPost = $this->postFactory->create();
        $newPost->setContent('hjdgshjdghjfdghfjdgshjfdgshjfdgashfj dasjghj dgashfj dgas hfj dgfkjb');
        $newPost->setIdentifier('test_09');
        $newPost->setIsActive('1');
        $newPost->setTitle('Test 09');
        $this->postRepository->save($newPost);
//        exit();
        
        // LOAD VIA REPOSITORY
        $post = $this->postRepository->getById(1);
        
        echo $post->getId()."<br>";
        echo $post->getTitle()."<br>";
        echo "<br>";
        echo get_class($post);
        
        
        
        // DELETE VIA REPOSITORY
//        $postToDelete = $this->postRepository->getById(5);
//        $this->postRepository->delete($postToDelete);
        
//        $this->postRepository->deleteById(5);
        
        // UPDATE VIA REPOSITORY
        $postToUpdate = $this->postRepository->getById(4);
        $postToUpdate->setTitle('ssssssssssssss333');
        $this->postRepository->save($postToUpdate);
//        exit();
        
        // GET LIST FROM REPOSITORY

        // FILTERING
        $this->searchCriteriaBuilder->addFilter('is_active', '1');
        
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $result = $this->postRepository->getList($searchCriteria);
        
        foreach ($result->getItems() as $item) {
            echo $item->getId()."<br>";
            echo $item->getTitle()."<br>";
            echo "<br>";
        }
        
        exit();
    }
}
