<?php

namespace Training\PlayWithApi\Controller\Index;


class Page extends \Magento\Framework\App\Action\Action
{
    private $pageResultFactory;
    private $pageCollectionFactory;
    private $pageResource;
    private $pageFactory;
    private $pageDataFactory;
    private $pageRepository;
    private $customerRepository;
    private $searchCriteriaBuilder;
    private $filterBuilder;
    private $sortOrderBuilder;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory,
        \Magento\Cms\Model\ResourceModel\Page\CollectionFactory $pageCollectionFactory,
        \Magento\Cms\Model\ResourceModel\Page $pageResource,
        \Magento\Cms\Model\PageFactory $pageFactory,
        \Magento\Cms\Api\Data\PageInterfaceFactory $pageDataFactory,
        \Magento\Cms\Api\PageRepositoryInterface $pageRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository,
        \Magento\Framework\Api\SearchCriteriaBuilder $searchCriteriaBuilder,
        \Magento\Framework\Api\FilterBuilder $filterBuilder,
        \Magento\Framework\Api\SortOrderBuilder $sortOrderBuilder
        
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
        $this->pageCollectionFactory = $pageCollectionFactory;
        $this->pageResource = $pageResource;
        $this->pageFactory = $pageFactory;
        $this->pageDataFactory = $pageDataFactory;
        $this->pageRepository = $pageRepository;
        $this->customerRepository = $customerRepository;
        $this->searchCriteriaBuilder = $searchCriteriaBuilder;
        $this->filterBuilder = $filterBuilder;
        $this->sortOrderBuilder = $sortOrderBuilder;
    }
    
    public function execute()
    {
        // GET LIST FROM REPOSITORY

        // FILTERING
//        $this->searchCriteriaBuilder->addFilter('page_layout', '1column');
//        $this->searchCriteriaBuilder->addFilter('is_active', '1');
        
        
        $filterPageLayout = $this->filterBuilder
            ->setField('page_layout')
            ->setValue('1column')
            ->setConditionType('eq')
            ->create();
        
        $filterIsActive = $this->filterBuilder
            ->setField('is_active')
            ->setValue('0')
            ->setConditionType('eq')
            ->create();
        
        $this->searchCriteriaBuilder->addFilters([$filterPageLayout, $filterIsActive]);
        
        // SORTING
        $this->sortOrderBuilder->setField('title')->setAscendingDirection();
        $sortOrder = $this->sortOrderBuilder->create();
        $this->searchCriteriaBuilder->addSortOrder($sortOrder);
        
        // PAGINATION
        $this->searchCriteriaBuilder->setCurrentPage(3);
        $this->searchCriteriaBuilder->setPageSize(2);
        
        $searchCriteria = $this->searchCriteriaBuilder->create();
        $result = $this->pageRepository->getList($searchCriteria);
        
        foreach ($result->getItems() as $item) {
            echo $item->getId()."<br>";
            echo $item->getTitle()."<br>";
            echo "<br>";
        }
        
        exit();
        
        // LOAD VIA RESOURCE MODEL
        $page = $this->pageFactory->create();
        $this->pageResource->load($page, 2);
        
        echo $page->getId()."<br>";
        echo $page->getTitle()."<br>";
        echo "<br>";
        
        // LOAD VIA REPOSITORY
        $page = $this->pageRepository->getById(2);
        
        echo $page->getId()."<br>";
        echo $page->getTitle()."<br>";
        echo "<br>";
        echo get_class($page);
        
        
        // CREATE VIA REPOSITORY
        $newPage = $this->pageDataFactory->create();
        $newPage->setContent('hjdgshjdghjfdghfjdgshjfdgshjfdgashfj dasjghj dgashfj dgas hfj dgfkjb');
        $newPage->setIdentifier('test_04');
        $newPage->setIsActive('0');
        $newPage->setTitle('Test 04');
        $newPage->setPageLayout('1column');
        $this->pageRepository->save($newPage);
        
        // DELETE VIA REPOSITORY
//        $pageToDelete = $this->pageRepository->getById(5);
//        $this->pageRepository->delete($pageToDelete);
        
//        $this->pageRepository->deleteById(5);
        
        // UPDATE VIA REPOSITORY
        $pageToUpdate = $this->pageRepository->getById(6);
        $pageToUpdate->setContentHeading('ssssssssssssss');
        $this->pageRepository->save($pageToUpdate);
        
        
        exit();
        
        // LOAD CUSTOMER VIA REPOSITORY
        $customer = $this->customerRepository->getById(1);
        
//        $customer->reset();
        echo "<br>";
        echo get_class($customer);
        
//        Magento\Customer\Model\Data\Customer
//        Magento\Customer\Model\Customer
        
        
        exit();
        /*
        $pageCollection = $this->pageCollectionFactory->create();
        $pageCollection->addOrder('page_id');
//        $pageCollection->addFieldToFilter('page_id', 2);
//        $pageCollection->addFieldToFilter('page_id', ['eq' => 2]);
//        $pageCollection->addFieldToFilter('page_id', ['in' => [2,3]]);
        $pageCollection->addFieldToSelect(['title', 'identifier']);
        $pageCollection->setPageSize(2);
        $pageCollection->setCurPage(2);
        
        echo $pageCollection->getSelect();
        echo "<br>";
        foreach ($pageCollection as $page) {
            echo $page->getId()."<br>";
            echo $page->getTitle()."<br>";
            echo "<br>";
        }
        
        echo $pageCollection->getSelect();
        */
        exit();
    }
}
