<?php

namespace Training\PlayWithApi\Controller\Index;


class Attribute extends \Magento\Framework\App\Action\Action
{
    private $pageResultFactory;
    private $productRepository;
    private $customerRepository;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
        \Magento\Framework\View\Result\PageFactory $pageResultFactory,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\Customer\Api\CustomerRepositoryInterface $customerRepository
    ) {
        parent::__construct($context);
        $this->pageResultFactory = $pageResultFactory;
        $this->productRepository = $productRepository;
        $this->customerRepository = $customerRepository;
    }
    
    public function execute()
    {
        
        $product = $this->productRepository->getById(1);
        
        // CUSTOM ATTRIBUTES
        $customAttributes = $product->getCustomAttributes();
        foreach ($customAttributes as $customAttribute) {
            echo $customAttribute->getAttributeCode().'<br>';
            print_r($customAttribute->getValue());
            echo '<br>';
            echo '<br>';
        } 
        echo '<br>';
        echo '<br>';
        
        $customAttribute = $product->getCustomAttribute('meta_keyword');
        echo $customAttribute->getAttributeCode().'<br>';
        print_r($customAttribute->getValue());
        echo '<br>';
        
        $customAttribute = $product->getCustomAttribute('screen_size');
        if ($customAttribute) {
            echo $customAttribute->getAttributeCode().'<br>';
            print_r($customAttribute->getValue());
        }
        
        // EXTENSION ATTRIBUTE
        echo '<br>*********************<br>';
        print_r($product->getExtensionAttributes()->getScreenSize());
        
        
        $product->getExtensionAttributes()->setScreenSize(6);
        $this->productRepository->save($product);
        
        exit();
    }
}
