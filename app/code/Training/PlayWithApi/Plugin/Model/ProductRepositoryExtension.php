<?php

namespace Training\PlayWithApi\Plugin\Model;

class ProductRepositoryExtension
{
    private $productExtensionFactory;

    public function __construct(
        \Magento\Catalog\Api\Data\ProductExtensionFactory $productExtensionFactory
    ) {
        $this->productExtensionFactory = $productExtensionFactory;
    }

    public function afterGetById(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $result,
        $productId, $editMode = false, $storeId = null, $forceReload = false
    ) {
        $this->loadScreenSizeAttributeData($result);
        return $result;
    }

    public function afterGet(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $result,
        $sku, $editMode = false, $storeId = null, $forceReload = false
    ) {
        $this->loadScreenSizeAttributeData($result);
        return $result;
    }
    
    private function loadScreenSizeAttributeData(\Magento\Catalog\Api\Data\ProductInterface $product)
    {
        $extension = $product->getExtensionAttributes();
        if (!$extension) {
            $extension = $this->productExtensionFactory->create();
        }
        $screenSizeAttribute = $product->getCustomAttribute('screen_size');
        if ($screenSizeAttribute) {
            $screenSize = $screenSizeAttribute->getValue();
            $extension->setScreenSize($screenSize);
        }
        
        $product->setExtensionAttributes($extension);
    }

    public function beforeSave(
        \Magento\Catalog\Api\ProductRepositoryInterface $subject,
        \Magento\Catalog\Api\Data\ProductInterface $product,
        $saveOptions = false
    ) {
        $extension = $product->getExtensionAttributes();
        if (!$extension) {
            $extension = $this->productExtensionFactory->create();
        }
        
        $screenSize = $extension->getScreenSize();
        
        $product->setCustomAttribute('screen_size', $screenSize); // Still have bug in Magento
//        $product->setData('screen_size', $screenSize);
    }
}
